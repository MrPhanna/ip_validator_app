﻿using System;
using System.Net;

namespace IP_Validator_App
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ConsoleKeyInfo key;
            do
            { 
                Console.Write("please input an IP Address : ");
                string? ip = Console.ReadLine();
                Console.WriteLine("The result is : " + ValidateIP(ip));
                Console.Write("Press any keys to continue or press Esc to exit: \r\n");

                key = Console.ReadKey(true);

            } while (key.Key != ConsoleKey.Escape);

        }

        public static bool ValidateIP(string ipAddress)
        {
            IPAddress address;
            if (!IPAddress.TryParse(ipAddress, out address))
            {
                return false;
            }
            string[] adrs = ipAddress.Split('.');
            if (adrs.Length != 4)
            {
                return false;
            }

            for (int i = 0; i < adrs.Length; i++)
            {
                if (adrs[i][0] == '0')
                {
                    return false;
                }

                int value;
                int.TryParse(adrs[i], out value);
                if (value > 255)
                {
                    return false;
                }
            }
            return true;
        }
    }
}